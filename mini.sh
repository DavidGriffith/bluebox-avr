#!/bin/sh

minipro -p attiny85 -w bluebox.hex -c code
minipro -e -p attiny85 -w fuses.txt -c config
minipro -e -p attiny85 -w bluebox.eep.hex -c data
